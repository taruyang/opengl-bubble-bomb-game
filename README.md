# Bubble Bomb #
**Jongho Yang (Joseph.Yang.MIITP@gmail.com)**
## Introduction ##
The program is a OpenGL 3D battle game written in C++. 
Though this document, major elements composing the game and environment would be introduced.

## Major Elements ##
### Bubble Bomb ###
This is implemented with the Icosphere objects with Tessellation shader. 
A spacecraft drops bombs during its movement and the bubble bombs move to the game player who is a viewer. 
The velocity and color of each bomb are in random order. 
The values are decided when the bomb is dropped. 
By completing stages, the speed of bombs will increase for the level of difficulty.

### Spacecraft ###
This is an enemy to combat with in this game. 
It is implemented with a mesh object internally. 
The used 3D model came from the free 3D site, https://free3d.com. 
It includes lighting maps which are consist of a diffuse map and a specula map for lighting. 
An original file is slightly modified to be used in Linux filesystem for this assignment. 
Like the bubble bomb, the speed and intensity of movement change as times go on and at the higher level. 
The pattern of movement is a composition of trigonometrical functions and differs as the level goes high. 
Special effect of this shuttle is that the surface color will be changed to red when it is damaged by the weapon of our combat plane.

### Game Player ###
The viewer of this game is a game player to battle with the spacecraft. 
The position of the player can be moved with the ‘W’, ‘S’ keys on a key-board and connected mouse input. 
A player can change the heading of flight with mouse movements. 
A player can move forward or backward to the decided direction by ‘W’ or ‘S” key, respectively. 
Attacking to the spacecraft and bomb is done by the space key. A player can destroy bombs for protection also. 
When a player is attacked by a bubble bomb, a notification message which is “Damaged !!!” will be displayed on screen. 
The cockpit of our fighter is drawn with orthographic projection and OpenGL blending.

### Information ###
Current game stage, remain energy of a player and the spacecraft are shown at the up-right corner of the game screen with blue color. In addition, when a stage is completed or a game is over, messages will be displayed at the center of the screen. 2D text messages are rendered as texture objects with the FreeType library. For displaying texts on screen at fixed positions regardless of camera movements, orthographic projection is used and OpenGL blending also.

### Planets ###
This is a kind of background objects to indicate that the fighting place is the cosmos. A planet is constructed by a mesh model internally from the free 3D site, https://free3d.com. It contains lighting maps for lighting effects. To place many planets without performance reduction, the instanced rendering is applied. The position, size and rotation angle of each plant are decided with a random function when it is instantiated.

### Lights ###
For lighting effects, total 7 lights are installed in this game. 

## Screenshots ##
![1.png](https://bitbucket.org/repo/4p8aMx8/images/3743304073-1.png)
### Figure 1 Stage start notification ###
![2.png](https://bitbucket.org/repo/4p8aMx8/images/604794926-2.png)
### Figure 2 The color change when the spacecraft is attacked ###
![3.png](https://bitbucket.org/repo/4p8aMx8/images/1382970266-3.png)
### Figure 3 Shot Mark by a player's weapon ###
![4.png](https://bitbucket.org/repo/4p8aMx8/images/1174434911-4.png)
### Figure 4 Planets ###
![5.png](https://bitbucket.org/repo/4p8aMx8/images/2065088237-5.png)
### Figure 5 Bubble bombs ###
![6.png](https://bitbucket.org/repo/4p8aMx8/images/3081461402-6.png)
### Figure 6 A notification when a player is damaged by a bubble bomb ###
![7.png](https://bitbucket.org/repo/4p8aMx8/images/2536079624-7.png)
### Figure 7 Game Over ###

## Directories ##
Please let you refer the below directories for this assignment.

- “doxygen”: Document folder. Please let you open doxygen/html/index.html file.
- “glsl”: Shaders used for this game programming.
- “resource”: Images used for texture and a mesh for spacecraft. 
- “font”: font file folder 

## Compile and running ##
The project is made with Code::Blocks on Ubutu linux environment.
Please let you excute "PlayGround.cbp" under Assignment folder.

## Notification ##
The below site has been referred to find out detail explanation for mesh model and 2D text rendering while doing this assignment. However, all codes are own implementation with some ideas and explanation on the site.
https://learnopengl.com/

## Reference ##
Patreon. (2015). Learn OpenGL. Retrieved from https://learnopengl.com/
Free3D. (2015). E 45 Aircraft. Retrieved from https://free3d.com/3d-model/e-45-aircraft-71823.html